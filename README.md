# Google_Cloud

Proyecto Google Cloud para el Bootcamp de Keepcoding

ID del Proyecto: ageless-courier-271712

1ª: Creamos proyecto, damos permisos y avisos de facturación

2ª: Creamos BBDD MySQL, configuramos copias, creamos usuarios y las dos BBDD indicadas.
    Exportamos, importamos BBDD indicadas, verificamos con los logs que als acciones se han realizado y descalamos la máquina

3ª: Creamos instancia, eliminamos instancia y mediante el disco una captura.(snapshot-apache)
    Generamos imagen(imageapache) con la captura del disco, con esto, creamos la plantilla (plantilla-apachepro)
    y creamos el grupo de instancia para que autoescale.(apache-balance)
    Creamos máquina virtual (agente), con script para verificar el autoescalado mediante cpu, que se encuentra en el path:
    /opt/script, fichero: Testeo_Cpu_Autoscaling.sh. Acceder como root.

4ª  Deployamos aplicación mediante GEA Standar.
    Configuramos app.yaml con datos de nuestra BBDD
    Verificamos acceso a la web
    Se realizan los deploys. Solo he modificado el servicio en el .yaml, ya que no me dejaba modificar la versión. Para modificar la versión lo he 
    realizado, mediante gcloud app deploy, indicandole la versión.
    